import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../model"
import "../service"


Dialog {
    property var expenseId
    property int expenseType
    canAccept: sumField.acceptableInput && sumField.text.length > 0

    id: page

    CategoriesListModel {
        id: categoriesListModel
    }
    SilicaFlickable {
        id: listView
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id:column
            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Expense")
            }
            ValueButton {
                property date date
                id: datePicker
                x: Theme.paddingLarge
                label: qsTr("Date")
                width: parent.width
                onClicked: openDateDialog()

                function openDateDialog() {
                    var dialog = pageStack.push("Sailfish.Silica.DatePickerDialog", {date: date})
                    dialog.accepted.connect(function() {
                        value = dialog.dateText
                        date = dialog.date
                    })
                }
            }
            TextField {
                id: sumField
                enabled: expenseType === TransactionTypeEnum.Expense
                x: Theme.paddingLarge
                width: page.width/2
                color: "white"
                inputMethodHints: Qt.ImhDigitsOnly
                placeholderText: qsTr("Sum")
                onClicked: {
                    sumField.validator = validator;
                }
            }
            IntValidator {
                id: validator
                bottom: 1
            }
            TextField {
                id: descriptionField
                x: Theme.paddingLarge
                width: page.width - Theme.paddingLarge * 2
                placeholderText: qsTr("Description")
            }
            ComboBox {
                property var dataModel : categoriesListModel
                id: expenseCategoryComboBox
                x: Theme.paddingLarge
                width: page.width/1.5
                label: qsTr("Category")
                menu: ContextMenu {
                        Repeater {
                            model: expenseCategoryComboBox.dataModel

                            MenuItem {
                                text: model.name
                                truncationMode: TruncationMode.Fade
                            }
                        }
                    }

                function displayCategories() {
                    expenseCategoryComboBox.dataModel.clear();
                    dao.retrieveCategories(expenseType, function(categories) {
                        for (var i = 0; i < categories.length; i++) {
                            var category = categories.item(i);
                            expenseCategoryComboBox.dataModel.addCategory(category.id, category.name)
                        }
                    });
                }

                function selectCategory(id) {
                    expenseCategoryComboBox.currentIndex = expenseCategoryComboBox.dataModel.getIndexById(id)
                }

                Component.onCompleted: displayCategories()
            }
            Button {
                id: addExpenseCategory
                visible: expenseType === TransactionTypeEnum.Expense
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.paddingLarge * 2
                    rightMargin: Theme.paddingLarge * 2
                }
                text: qsTr("Add a new category")
                color: Theme.primaryColor
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("AddCategory.qml"), { categoryType: expenseType})
                    dialog.accepted.connect ( function () {
                        expenseCategoryComboBox.displayCategories()
                    });
                }
            }/*
            ComboBox {
                id: imp
                x: Theme.paddingLarge * 2
                width: page.width/1.8
                label: qsTr("Важность")
                menu: ContextMenu {
                    MenuItem { text: qsTr("Важно") }
                    MenuItem { text: qsTr("Неважно") }
                }
            }
            Button{
                id: calculator
                x: Theme.paddingLarge * 2.8
                text: qsTr("Калькулятор")
                width: page.width/1.4
            }
            Button {
                id: photo
                x: Theme.paddingLarge * 2.8
                text: qsTr("Добавить фото чека")
                width: page.width/1.4
           }*/
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            if (expenseId === undefined) {
                dao.createTransaction(datePicker.date.toISOString(), sumField.text,
                                      expenseCategoryComboBox.dataModel.get(expenseCategoryComboBox.currentIndex).id,
                                      TransactionTypeEnum.Expense, descriptionField.text);
            } else {
                dao.updateTransaction(expenseId, datePicker.date.toISOString(), sumField.text,
                                      expenseCategoryComboBox.dataModel.get(expenseCategoryComboBox.currentIndex).id,
                                      descriptionField.text);
            }
        }
    }

    Component.onCompleted: {
        if (expenseId === undefined) {
            datePicker.date = new Date()
            datePicker.value = datePicker.date.toLocaleDateString(Locale.ShortFormat)
        } else {
            dao.retrieveTransactionById(expenseId, function(expense) {
                sumField.text = expense.sum
                descriptionField.text = expense.description
                datePicker.date = new Date(expense.date)
                datePicker.value =  datePicker.date.toLocaleDateString(Locale.ShortFormat)
                expenseCategoryComboBox.selectCategory(expense.category_id)
            })
        }
    }
}
