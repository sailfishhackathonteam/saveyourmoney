import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../service"
import "../"

Dialog {
    property int categoryType
    id: page
    canAccept: categoryName.acceptableInput  && categoryName.text.length > 0

    Dao {
        id: dao
    }

    SilicaFlickable {
        id: listView
        anchors.fill: parent
        contentHeight: column.height
    }
    Column{
        PageHeader {
            title: qsTr("Add category")
        }
        id:column
        width: page.width
        spacing: Theme.paddingLarge
        TextField{
            id: categoryName
            x: Theme.paddingLarge
            width: page.width - Theme.paddingLarge *2
            placeholderText: qsTr("New category")
            onClicked: {
                categoryName.validator = validator;
            }
        }
        RegExpValidator {
            id: validator
            regExp: /[a-zA-Z, а-яА-Я, 0-9]{1,200}/
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            dao.createCategory(categoryName.text, categoryType);
        }
    }
}

