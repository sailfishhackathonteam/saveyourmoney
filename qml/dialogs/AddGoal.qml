import QtQuick 2.0
import Sailfish.Silica 1.0
import "../"
import "../service"

Dialog {
    property var goalId
    id: page
    canAccept: sumField.acceptableInput && nameField.acceptableInput
               && nameField.text.length > 0 && sumField.text.length > 0

    SilicaFlickable {
        id: listView
        anchors.fill: parent
        contentHeight: column.height
    }
    Column{
        PageHeader {
            id: header
            title: qsTr("Add goal")
        }
        id:column
        width: page.width
        spacing: Theme.paddingLarge
        TextField{
            id: nameField
            x: Theme.paddingLarge
            width: page.width - Theme.paddingLarge * 2
            placeholderText: qsTr("Name of goal")
            onClicked: {
                nameField.validator = nameValidator;
            }
        }
        RegExpValidator {
            id: nameValidator
            regExp: /[a-zA-Z, а-яА-Я, 0-9]{1,200}/
        }
        TextField{
            id: sumField
            inputMethodHints: Qt.ImhDigitsOnly
            x: Theme.paddingLarge
            width: page.width/2
            placeholderText: qsTr("Sum")
            onClicked: {
                sumField.validator = sumValidator;
            }
        }
        IntValidator {
            id: sumValidator
            bottom: 1
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            if (goalId === undefined) {
                dao.createGoal(nameField.text, sumField.text);
            } else {
                dao.updateGoal(goalId, nameField.text, sumField.text)
            }
        }
    }

    Component.onCompleted: {
        if (goalId !== undefined) {
            header.title = qsTr("Edit goal");
            dao.retrieveGoalById(goalId, function(goal) {
                nameField.text = goal.name;
                sumField.text = goal.sum;
            });
        }
    }
}


