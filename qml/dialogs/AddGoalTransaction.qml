import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../model"
import "../service"

Dialog {
    property var goalTransactionId
    property var goalId
    property var date
    property var sum

    canAccept: sumField.acceptableInput && sumField.text.length > 0

    id: page

    SilicaFlickable {
        id: listView
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id:column
            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Increase goal progress")
            }
            ValueButton {
                property date selectedDate
                id: datePicker
                x: Theme.paddingLarge
                label: qsTr("Date")
                width: parent.width
                onClicked: openDateDialog()

                function openDateDialog() {
                    var dialog = pageStack.push("Sailfish.Silica.DatePickerDialog", {date: selectedDate})
                    dialog.accepted.connect(function() {
                        value = dialog.dateText
                        selectedDate = dialog.date
                    })
                }
            }
            TextField {
                id: sumField
                inputMethodHints: Qt.ImhDigitsOnly
                x: Theme.paddingLarge
                width: page.width / 2
                placeholderText: qsTr("Sum")
                onClicked: {
                    sumField.validator = validator;
                }
            }
            IntValidator {
                id: validator
                bottom: 1
            }
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            date = datePicker.selectedDate
            sum = parseInt(sumField.text)
        }
    }

    Component.onCompleted: {
        if (goalTransactionId === undefined) {
            datePicker.selectedDate = new Date()
            datePicker.value = datePicker.selectedDate.toLocaleDateString(Locale.ShortFormat)
        } else {
            sumField.text = sum
            datePicker.selectedDate = date
            datePicker.value =  date.toLocaleDateString(Locale.ShortFormat)
        }
    }
}



