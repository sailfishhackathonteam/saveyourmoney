import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../model"
import "../service"

Dialog {
    property var incomeId
    canAccept: sumField.acceptableInput && sumField.text.length > 0
    id: page

    CategoriesListModel {
        id: categoriesListModel
    }

    SilicaFlickable {
        id: listView
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id:column
            width: page.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Income")
            }
            ValueButton {
                property date date
                id: datePicker
                x: Theme.paddingLarge
                label: qsTr("Date")
                width: parent.width
                onClicked: openDateDialog()

                function openDateDialog() {
                    var dialog = pageStack.push("Sailfish.Silica.DatePickerDialog", {date: date})
                    dialog.accepted.connect(function() {
                        value = dialog.dateText
                        date = dialog.date
                    })
                }
            }
            TextField {
                id: sumField
                inputMethodHints: Qt.ImhDigitsOnly
                x: Theme.paddingLarge
                width: page.width - Theme.paddingLarge * 2
                placeholderText: qsTr("Sum")
                onClicked: {
                    sumField.validator = sumValidator;
                }
            }
            IntValidator {
                id: sumValidator
                bottom: 1
            }
            TextField {
                id: descriptionField
                x: Theme.paddingLarge
                width: page.width - Theme.paddingLarge * 2
                placeholderText: qsTr("Description")
            }
            ComboBox {
                property var dataModel : categoriesListModel
                id: incomeCategoryComboBox
                width: page.width/1.5
                x: Theme.paddingLarge
                label: qsTr("Category")
                Component.onCompleted: displayCategories()
                onCurrentIndexChanged: console.log(currentIndex)
                menu: ContextMenu {
                        Repeater {
                            model: incomeCategoryComboBox.dataModel
                            MenuItem {
                                text: model.name
                            }
                        }
                    }

                function displayCategories() {
                    incomeCategoryComboBox.dataModel.clear();
                    dao.retrieveCategories(TransactionTypeEnum.Income, function(categories) {
                        for (var i = 0; i < categories.length; i++) {
                            var category = categories.item(i);
                            incomeCategoryComboBox.dataModel.addCategory(category.id, category.name)
                        }
                    });
                }

                function selectCategory(id) {
                    incomeCategoryComboBox.currentIndex = incomeCategoryComboBox.dataModel.getIndexById(id);
                }
            }
            Button {
                id: addIncomeCategory
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.paddingLarge * 2
                    rightMargin: Theme.paddingLarge * 2
                }
                text: qsTr("Add a new category")
                color: Theme.primaryColor
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("AddCategory.qml"), { categoryType: TransactionTypeEnum.Income })
                    dialog.accepted.connect ( function () {
                        incomeCategoryComboBox.displayCategories()

                    });
                }
            }
        }
    }

    onDone: {
        if (result == DialogResult.Accepted) {
            if (incomeId === undefined) {
                dao.createTransaction(datePicker.date.toISOString(), sumField.text,
                                      incomeCategoryComboBox.dataModel.get(incomeCategoryComboBox.currentIndex).id,
                                      TransactionTypeEnum.Income, descriptionField.text);
            } else {
                dao.updateTransaction(incomeId, datePicker.date.toISOString(), sumField.text,
                                      incomeCategoryComboBox.dataModel.get(incomeCategoryComboBox.currentIndex).id,
                                      descriptionField.text);
            }
        }
    }

    Component.onCompleted: {
        if (incomeId === undefined) {
            datePicker.date = new Date()
            datePicker.value = datePicker.date.toLocaleDateString(Locale.ShortFormat)
        } else {
            dao.retrieveTransactionById(incomeId, function(income) {
                sumField.text = income.sum
                descriptionField.text = income.description
                datePicker.date = new Date(income.date)
                datePicker.value =  datePicker.date.toLocaleDateString(Locale.ShortFormat)
                incomeCategoryComboBox.selectCategory(income.category_id)
            })
        }
    }
}


