import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../service"

Dialog {
    id: page
    canAccept: true

     SilicaFlickable {
         id: listView
        anchors.fill: parent
        contentHeight: column.height
        Column {
            id:column
            width: page.width
            spacing: Theme.paddingLarge
            PageHeader {
                title: qsTr("Date interval")
            }
            ComboBox {
                id: variantDate
                x: Theme.paddingLarge
                width: page.width/1.5
                label: qsTr("Display for:")
                currentIndex: dao.currentIndexComboBoxDate;

                menu: ContextMenu {
                    MenuItem { text: qsTr("Week")}
                    MenuItem { text: qsTr("Month") }
                    MenuItem { text: qsTr("Three months") }
                    MenuItem { text: qsTr("Year") }
                    MenuItem { text: qsTr("For all time") }
                    MenuItem { text: qsTr("Custom") }
                }

                onCurrentIndexChanged: setDatesFromComboBox(currentIndex)

                function setDatesFromComboBox(index) {
                    var date = new Date();
                    dao.currentIndexComboBoxDate = index;
                    switch(index) {
                    case 0: var day = date.getDay();
                            dao.startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + (day == 0?-6:1)-day );
                            dao.endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() + (day == 0?0:7)-day );
                            refreshValueOfDatePickers();
                            break;
                    case 1: dao.startDate = new Date(date.getFullYear(), date.getMonth(), 1);
                            dao.endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                            refreshValueOfDatePickers();
                            break;

                    case 2: dao.startDate =  new Date(date.getFullYear(), date.getMonth() - 2, 1);
                            dao.endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                            refreshValueOfDatePickers();
                            break;

                    case 3: dao.startDate = new Date(date.getFullYear() - 1, date.getMonth(), 1);
                            dao.endDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                            refreshValueOfDatePickers();
                            break;
                    case 4: dao.retrieveDateOfFirstTransaction(function(result){
                                        dao.startDate =  new Date(result);
                                    });
                            dao.retrieveDateOfLastTransaction(function(result){
                                                                dao.endDate =  new Date(result);
                                                            });
                            refreshValueOfDatePickers();
                            break;
                    }
                }

                function refreshValueOfDatePickers() {
                    startDatePicker.selectedStartDate = dao.startDate;
                    endDatePicker.selectedFinishDate = dao.endDate;
                    startDatePicker.value = dao.startDate.toLocaleDateString(Locale.ShortFormat)
                    endDatePicker.value = dao.endDate.toLocaleDateString(Locale.ShortFormat)
                    isDateOK();
                }
            }

            ValueButton {
                property date selectedStartDate
                id: startDatePicker
                x: Theme.paddingLarge
                label: qsTr("Start date")
                value: dao.startDate.toLocaleDateString(Locale.ShortFormat)
                width: parent.width
                onClicked: {
                    variantDate.currentIndex = 5;
                    openDateDialog()
                }

                function openDateDialog() {
                    var dialog = pageStack.push("Sailfish.Silica.DatePickerDialog", {date: selectedStartDate})
                    dialog.accepted.connect(function() {
                        value = dialog.dateText
                        dao.startDate = dialog.date
                        isDateOK();
                    })
                }
            }
            ValueButton {
                property date selectedFinishDate
                id: endDatePicker
                x: Theme.paddingLarge
                label: qsTr("End date")
                value: dao.endDate.toLocaleDateString(Locale.ShortFormat)
                width: parent.width
                onClicked: {
                    variantDate.currentIndex = 5;
                    openDateDialog()
                }

                function openDateDialog() {
                    var dialog = pageStack.push("Sailfish.Silica.DatePickerDialog", {date: selectedFinishDate})
                    dialog.accepted.connect(function() {
                        value = dialog.dateText
                        dao.endDate = dialog.date
                        isDateOK();
                    })
                }
            }

            Label {
                id: ifDateIncorrect
                anchors.horizontalCenter: parent.horizontalCenter
                color: Theme.secondaryHighlightColor

            }
        }
     }

     function isDateOK() {
         ifDateIncorrect.text = qsTr("")
         if(dao.startDate < dao.endDate) {
             canAccept = true;
         } else {
             canAccept = false
             ifDateIncorrect.text = qsTr("The end date is later than a start date")
         }
     }

     onDone: dao.updateStatisticsValue()

     Component.onCompleted: {
         endDatePicker.selectedFinishDate = dao.endDate
         endDatePicker.value = dao.endDate.toLocaleDateString(Locale.ShortFormat)
         startDatePicker.selectedStartDate = dao.startDate
         startDatePicker.value = dao.startDate.toLocaleDateString(Locale.ShortFormat)
     }
}

