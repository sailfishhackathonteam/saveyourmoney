import QtQuick 2.0

ListModel {

    function addGoal(id, name,sum, reachedSum) {
        append({
                   id: id,
                   name: name,
                   sum: sum,
                   reachedSum: reachedSum
               });
    }

    function updateGoal(index, name, sum, reachedSum) {
        set(index, {name: name, sum: sum, reachedSum: reachedSum})
    }
}
