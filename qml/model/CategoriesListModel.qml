import QtQuick 2.0

ListModel {

    id: categoriesListModel

    function addCategory(id, name) {
        append({
                   id: id,
                   name: name
               });
    }

    function getIndexById(id) {
        for (var index = 0; index < count; index++) {
            var category = get(index)
            if (category.id === id) {
                return index
            }
        }
    }
}

