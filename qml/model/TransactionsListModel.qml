import QtQuick 2.0

ListModel {

    function addTransaction(id, date, sum, category, type, description) {
        append({
                   id: id,
                   date: date,
                   sum: sum,
                   category: category,
                   type: type,
                   description: description
               });
    }

    function updateTransaction(index, date, sum, category, type, description) {
        set(index, {date: date, sum: sum, category: category, type: type, description: description})
    }
}

