import QtQuick 2.2
import Sailfish.Silica 1.0


Column {

    id: statisticField

    property string label
    property int value
    property string clickPath
    property string period
    property int transactionType
    property int ifShowButton

    width: parent.width

    Item {
        width: parent.width
        height: labelText.height

        Label {
            id: labelText
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            text: label
        }
        Label {
            anchors.top: parent.top;
            anchors.horizontalCenter: parent.horizontalCenter;
            color: Theme.secondaryHighlightColor
            text: value

        }
        IconButton {
                id: iconButton
                anchors.right: parent.right
                anchors.rightMargin: -Theme.horizontalPageMargin * 0.6
                anchors.verticalCenter: parent.verticalCenter
                anchors.top:   parent.top
                opacity: if(ifShowButton == 0) {0} else {100}
                icon.source: "image://theme/icon-m-right?" + (pressed
                             ? Theme.highlightColor
                             : Theme.primaryColor)
                onClicked: if(ifShowButton == 1) {
                              if (transactionType === undefined)  {
                               pageStack.push(Qt.resolvedUrl(clickPath))
                              } else {
                                  pageStack.push(Qt.resolvedUrl(clickPath), {transactionType: transactionType})
                              }
                          }
            }
    }
}

