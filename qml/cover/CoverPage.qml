import QtQuick 2.0
import Sailfish.Silica 1.0
import "../service"

CoverBackground {
    id: page

    Column {
        id: balance
        width: page.width
        anchors.verticalCenter: parent.verticalCenter

        Label {
            id: balanceName
            x: Theme.horizontalPageMargin
            text: qsTr("Balance:")
        }
        Label {
            id: balanceValue
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 50
            text: dao.balanceStatistics
        }
    }
}
