import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import harbour.save.your.money 1.0


Item {

    property var database
    property var expenseStatistics
    property var incomeStatistics
    property var balanceStatistics
    property var goalStatistics
    property var goalProgressStatistics

    property date startDate: getDate(true);
    property date endDate: getDate(false);

    property int currentIndexComboBoxDate: 1;

    Component.onCompleted: {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0")
        database.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS TransactionsTable(
                 id INTEGER PRIMARY KEY AUTOINCREMENT,
                 date TEXT,
                 sum INTEGER,
                 category_id INTEGER,
                 type INTEGER,
                 goal_id INTEGER,
                 description TEXT)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS CategoriesTable(
                 id INTEGER PRIMARY KEY AUTOINCREMENT,
                 name TEXT,
                 type INTEGER)");
            tx.executeSql("CREATE TABLE IF NOT EXISTS GoalTable(
                 id INTEGER PRIMARY KEY AUTOINCREMENT,
                 name TEXT,
                 sum INTEGER,
                 isFinished INTEGER)");
            var result = tx.executeSql("SELECT COUNT(*) as count FROM CategoriesTable").rows.item(0).count;
            if (result === 0) {
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Food"), TransactionTypeEnum.Expense]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Car"), TransactionTypeEnum.Expense]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Cafe"), TransactionTypeEnum.Expense]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Cellular"), TransactionTypeEnum.Expense]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("House"), TransactionTypeEnum.Expense]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Health"), TransactionTypeEnum.Expense]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Others"), TransactionTypeEnum.Expense]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Salary"), TransactionTypeEnum.Income]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Savings"), TransactionTypeEnum.Income]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Premium"), TransactionTypeEnum.Income]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Goal progress"), TransactionTypeEnum.Goal]);
                tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)",
                              [qsTr("Goal finish"), TransactionTypeEnum.FinishedGoal]);
            }
        });
        updateStatisticsValue();
    }

    function updateStatisticsValue() {
        if(currentIndexComboBoxDate === 4) {
            retrieveDateOfFirstTransaction(function(result){
                startDate = result;
            });
            retrieveDateOfLastTransaction(function(result){
                endDate = result;
            });
        }
        retrieveExpenseStatistics(startDate, endDate, function(result) {
            if (result !== null) {
                expenseStatistics = result * (-1);
            } else {
                expenseStatistics = 0;
            }
        });
        retrieveIncomeStatistics(startDate, endDate, function(result) {
            if (result !== null) {
                incomeStatistics = result
            } else {
                incomeStatistics = 0
            }
        });
        balanceStatistics = incomeStatistics + expenseStatistics;
        retrieveGoalStatistics(function(result) {
            if (result !== null) {
                goalStatistics = result;
            } else {
                goalStatistics = 0
            }
        });
        retrieveGoalProgressStatistics(function(result) {
            if (result !== null) {
                goalProgressStatistics = result;
            } else {
                goalProgressStatistics = 0
            }
        });
    }

    function retrieveDateOfFirstTransaction(callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT MIN(date(date)) as minDate FROM TransactionsTable");
            callback(result.rows.item(0).minDate)
        });
    }

    function retrieveDateOfLastTransaction(callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT MAX(date(date)) as maxDate FROM TransactionsTable");
            callback(result.rows.item(0).maxDate)
        });
    }

    function createTransaction(date, sum, category_id, type, description) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.transaction(function(tx) {
            tx.executeSql("INSERT INTO TransactionsTable(date, sum, category_id, type, description)
                                VALUES(?, ?, ?, ?, ?)", [date, sum, category_id, type, description]);
        });
        updateStatisticsValue();
    }

    function updateTransaction(id, date, sum, category_id, description) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.transaction(function(tx) {
            tx.executeSql("UPDATE TransactionsTable SET date = ?, sum = ?, category_id = ?, description = ?
                                 WHERE id = ?", [date, sum, category_id, description, id]);
        });
        updateStatisticsValue();
    }

    function removeTransaction(id) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.transaction(function(tx) {
            tx.executeSql("DELETE FROM TransactionsTable WHERE id = ?", [id]);
        });
        updateStatisticsValue();
    }

    function retrieveTransactionById(id, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT * FROM TransactionsTable WHERE id = ?", [id]);
            callback(result.rows.item(0));
        });
    }

    function retrieveTransactionsFromInterval(startDate, endDate, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql(
                        "SELECT TransactionsTable.id, date, TransactionsTable.sum, category_id, type, description
                         FROM TransactionsTable LEFT OUTER JOIN GoalTable
                         ON TransactionsTable.goal_id = GoalTable.id  WHERE
                         (type = 0 OR type = 1 OR type = 3 OR type = 2 AND isFinished = 0)
                         AND date(date) <= date(?) AND date(date) >= date(?)
                         ORDER BY date(date) DESC", [endDate, startDate]);
            callback(result.rows);
        });
    }

    function retrieveTransactionsFromIntervalByType(type, startDate, endDate, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var whereCondition;
            if(type === TransactionTypeEnum.Expense) {
                whereCondition = "(type = 0 OR type = 3 OR type = 2 AND isFinished = 0)";
            } else {
                whereCondition = "type = 1";
            }
            var result = tx.executeSql(
                        "SELECT TransactionsTable.id, date, TransactionsTable.sum, category_id, type, description
                         FROM TransactionsTable LEFT OUTER JOIN GoalTable
                         ON TransactionsTable.goal_id = GoalTable.id  WHERE "
                         + whereCondition + " AND date(date) <= date(?) AND date(date) >= date(?)
                         ORDER BY date(date) DESC ",
                        [endDate, startDate]);
            callback(result.rows);
        });
    }

    function createCategory(name, type) {
        database.transaction(function(tx) {
            tx.executeSql("INSERT INTO CategoriesTable(name, type) VALUES(?, ?)", [name, type]);
        });
    }


    function retrieveCategories(type, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT * FROM CategoriesTable WHERE type = ? ORDER BY id ASC", [type]);
            callback(result.rows)
        });
    }

    function retrieveCategoryNum(callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT NUM(id) AS num FROM CategoriesTable");
            callback(result.rows.item(0).num)
        });
    }

    function createGoal(name, sum) {
        database.transaction(function(tx) {
            tx.executeSql("INSERT INTO GoalTable(name, sum, isFinished) VALUES(?, ?, 0)", [name, sum]);
        });
        updateStatisticsValue();
    }

    function updateGoal(id, name, sum) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.transaction(function(tx) {
            tx.executeSql("UPDATE GoalTable SET name = ?, sum = ? WHERE id = ?", [name, sum, id]);
        });
        updateStatisticsValue();
    }

    function finishGoal(id) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.transaction(function(tx) {
            tx.executeSql("UPDATE GoalTable SET isFinished = 1 WHERE id = ?", [id]);
        });
        var reachedSum;
        retrieveTransactionSumByGoalId(id, function(result){
            reachedSum = result;
        });
        var goal;
        retrieveGoalById(id, function(result){
            goal = result;
        });
        createTransaction(new Date(), reachedSum, 12, TransactionTypeEnum.FinishedGoal, goal.name);
        updateStatisticsValue();
    }

    function removeGoal(id) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.transaction(function(tx) {
            tx.executeSql("DELETE FROM GoalTable WHERE id = ?", [id]);
        });
        database.transaction(function(tx) {
            tx.executeSql("DELETE FROM TransactionsTable WHERE goal_id = ?", [id]);
        });
        updateStatisticsValue();
    }

    function retrieveGoalById(id, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT * FROM GoalTable WHERE id = ?", [id]);
            callback(result.rows.item(0));
        });
    }

    function retrieveActiveGoals(callback) {
        retrieveGoals(false, callback);
    }

    function retrieveFinishedGoals(callback) {
        retrieveGoals(true, callback);
    }

    function retrieveGoals(isFinished, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT * FROM GoalTable WHERE isFinished = ? ORDER BY id ASC",
                                       [isFinished]);
            callback(result.rows)
        });
    }

    function createGoalTransaction(date, sum, goalId) {
        database.transaction(function(tx) {
            var goal;
            retrieveGoalById(goalId, function(result){
                goal = result;
            });
            tx.executeSql("INSERT INTO TransactionsTable(date, sum, category_id, type, goal_id, description)
                                VALUES(?, ?, 11, 2, ?, ?)", [date, sum, goalId, goal.name]);
        });
        updateStatisticsValue();
    }

    function updateGoalTransaction(id, date, sum) {
        var description;
        retrieveTransactionById(id, function(result){
            description = result.description;
        })
        updateTransaction(id, date, sum, 11, description);
    }

    function retrieveTransactionSumByGoalId(goalId, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT SUM(sum) AS reachedSum FROM TransactionsTable
                                            WHERE type = 2 AND goal_id = ?", [goalId]);
            callback(result.rows.item(0).reachedSum)
        });
    }

    function retrieveTransactionsByGoalId(goalId, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT * FROM TransactionsTable WHERE goal_id = ?
                                        ORDER BY date(date) DESC", [goalId]);
            callback(result.rows)
        });
    }


    function retrieveCategoryById(id, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT * FROM CategoriesTable WHERE id = ? ORDER BY id DESC  LIMIT 1", id);
            callback(result.rows.item(0))
        });
    }

    function retrieveCategorySetting(callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT * FROM SettingsTable WHERE id = 1 ORDER BY id DESC  LIMIT 1");
            callback(result.rows.item(0))
        });
    }

    function retrieveExpenseStatistics(startDate, endDate, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT SUM(sum) AS expenseSum FROM
                    (Select TransactionsTable.sum as sum FROM TransactionsTable LEFT OUTER JOIN GoalTable
                     ON TransactionsTable.goal_id = GoalTable.id
                     WHERE (type = 0 OR type = 3 OR type = 2 AND isFinished = 0)
                     AND date(date) <= date(?) AND date(date) >= date(?))", [endDate, startDate]);
            callback(result.rows.item(0).expenseSum)
        });
    }

    function retrieveGoalStatistics(callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT SUM(sum) AS goalSum FROM GoalTable WHERE isFinished = 0");
            callback(result.rows.item(0).goalSum)
        });
    }

    function retrieveIncomeStatistics(startDate, endDate, callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT SUM(sum) AS incomeSum FROM TransactionsTable
                            WHERE type = 1 AND date(date) <= date(?) AND date(date) >= date(?)",
                                       [endDate, startDate]);
            callback(result.rows.item(0).incomeSum)
        });
    }

    function retrieveGoalProgressStatistics(callback) {
        database = LocalStorage.openDatabaseSync("SaveYourMoneyDatabase", "1.0");
        database.readTransaction(function(tx) {
            var result = tx.executeSql("SELECT SUM(sum) AS goalProgress FROM
                            ( SELECT * FROM TransactionsTable INNER JOIN GoalTable
                            ON TransactionsTable.goal_id = GoalTable.id
                            WHERE type = 2 and isFinished = 0 )");
            callback(result.rows.item(0).goalProgress)
        });
    }

    /*
      Counting first day of last day of current month
      */
    function getDate(isStart) {
        var date = new Date();
        var currentDate;
        if(isStart) {
            currentDate = new Date(date.getFullYear(), date.getMonth(), 1);
        } else {
            currentDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
        }
        return currentDate;
    }
}

