import QtQuick 2.0
import Sailfish.Silica 1.0
import "../"

Page {
    id: page
    SilicaFlickable {
        Column {
            PageHeader {
                title: qsTr("Currency")
            }
            id:column
            width: page.width
            spacing: Theme.paddingLarge
            Label{
                text: qsTr("Currency")
            }
        }
    }
}
