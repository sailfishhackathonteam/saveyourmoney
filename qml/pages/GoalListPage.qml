import QtQuick 2.0
import Sailfish.Silica 1.0
import "../service"
import "../model"
import "../dialogs"

Page {
    id: page

    Label {
        id: ifNoGoals
        color: Theme.secondaryHighlightColor
        y: page.height / 2
        anchors.horizontalCenter: parent.horizontalCenter
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height
        PullDownMenu {
            MenuItem {
                text: qsTr("View finished goals")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("FinishedGoalListPage.qml"));
                }
            }
            MenuItem {
                text: qsTr("Add goal")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("AddGoal.qml"));
                    dialog.accepted.connect(function() {
                        displayGoals();
                    });
                }
            }
        }
        PageHeader {
            id: header
            title: qsTr("Goals")
            width: parent.width
        }
        Column {
            id: column
            width: parent.width
            anchors.top: header.bottom
            spacing: Theme.paddingLarge
            Label {
                id: label
                text: qsTr("Progress of all goals:")
                x: Theme.paddingLarge
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }
            }
            Label {
                id: goalProgress
                text: dao.goalProgressStatistics + "/" + dao.goalStatistics
                anchors {
                    left: parent.left
                    right: parent.right
                }
                font.pixelSize: Theme.fontSizeLarge
                horizontalAlignment: Text.AlignHCenter
                color: Theme.highlightColor
            }

            SilicaListView {
                id: listView
                model: GoalListModel { id: goalsListModel }
                width: parent.width
                VerticalScrollDecorator {}
                height: page.height - header.height - label.height - goalProgress.height
                        - Theme.paddingLarge * 2
                clip: true
                delegate: ListItem {
                    Label {
                        id: goalName
                        text: name
                        truncationMode: TruncationMode.Fade
                        anchors.left: parent.left
                        anchors.leftMargin: Theme.paddingLarge
                        anchors.right: parent.right
                        anchors.rightMargin: Theme.paddingLarge * (countDigits(sum) * 1.7)
                    }
                    Label {
                        id: sumLabel
                        text: reachedSum + "/" + sum
                        anchors.right: parent.right
                        anchors.rightMargin: Theme.paddingLarge
                    }
                    menu: ContextMenu {
                        MenuItem {
                            text: qsTr("Edit")
                            onClicked: {
                                var dialog = pageStack.push(Qt.resolvedUrl("AddGoal.qml"),
                                                            {goalId: id});
                                dialog.accepted.connect(function() {
                                    displayGoals();
                                })
                            }
                        }
                        MenuItem {
                            text: qsTr("Remove")
                            onClicked:  {
                                dao.removeGoal(id);
                                listView.model.remove(model.index);
                            }
                        }
                    }
                    onClicked: {
                        pageStack.push(Qt.resolvedUrl("GoalTransactionListPage.qml"),
                                       {goalId: id, goalSum: sum,goalReachedSum: reachedSum,
                                           goalName: name, isFinished: false});
                    }
                }
                Component.onCompleted: displayGoals()
            }
        }
    }
    function countDigits(n) {
       for(var i = 0; n > 1; i++) {
          n /= 10;
       }
       return i;
    }

    function displayGoals() {
        listView.model.clear();
        dao.retrieveActiveGoals(function(goals) {
            ifNoGoals.text = "";
            if(goals.length !== 0) {
                for (var i = 0; i < goals.length; i++) {
                    var goal = goals.item(i);
                    var reachedSum = 0;
                    dao.retrieveTransactionSumByGoalId(goal.id, function(result) {
                        if (result !== null) {
                            reachedSum = result;
                        }
                    })
                    listView.model.addGoal(goal.id, goal.name, goal.sum, reachedSum);
                }
            } else {
                ifNoGoals.text = qsTr("You don't have any actual goals right now");
            }
        });
    }
}
