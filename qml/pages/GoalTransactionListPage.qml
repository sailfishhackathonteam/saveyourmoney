import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../service"
import "../model"
import "../dialogs"

Page {
    property var goalId
    property var goalSum
    property var goalReachedSum
    property var goalName
    property var isFinished
    id: page

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height
        PullDownMenu {
            visible: !isFinished
            MenuItem {
                visible: !isFinished && goalReachedSum >= goalSum
                text: qsTr("Finish goal")
                onClicked: {
                    dao.finishGoal(goalId)
                    pageStack.replaceAbove(null, [Qt.resolvedUrl("StartPage.qml"),
                                                  Qt.resolvedUrl("TransactionsListPage.qml")])
                }
            }
            MenuItem {
                text: qsTr("Add progress")
                onClicked: {
                    var dialog =
                            pageStack.push(Qt.resolvedUrl("AddGoalTransaction.qml"), {goalId: goalId});
                    dialog.accepted.connect (function () {
                        dao.createGoalTransaction(dialog.date.toISOString(), dialog.sum, goalId);
                        listView.displayTransactions();
                        pageStack.previousPage(pageStack.previousPage()).displayGoals();
                        goalReachedSum += dialog.sum;
                    })
                }
            }
        }
        PageHeader {
            id: header
            title: qsTr("Description of the goal")
            width: parent.width
        }
        Column {
            id: column
            width: parent.width
            anchors.top: header.bottom
            spacing: Theme.paddingLarge
            Label {
                id: label
                text: goalName
                anchors {
                    left: parent.left
                    right: parent.right
                    margins: Theme.paddingLarge
                }
                wrapMode: Text.Wrap
                truncationMode: TruncationMode.Fade
                font.pixelSize: Theme.fontSizeHuge
                horizontalAlignment: Text.AlignHCenter
            }
            ProgressBar {
                id: goalProgress
                width: parent.width
                maximumValue: goalSum
                Label {
                    text: ((goalReachedSum / goalSum) * 100).toFixed(0) + "%"
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                value: goalReachedSum
                label: qsTr("Saved") + " " + goalReachedSum + " " +
                       qsTr("from") + " " + goalSum
            }

            SilicaListView {
                id: listView
                model: TransactionsListModel {}
                width: parent.width
                VerticalScrollDecorator {}
                height: page.height - header.height - label.height - goalProgress.height
                        - Theme.paddingLarge * 2
                clip: true
                delegate: ListItem {
                    Label {
                        anchors.left: parent.left
                        anchors.leftMargin: Theme.paddingLarge
                        id: dataLabel
                        text: date.toLocaleDateString(Locale.ShortFormat)
                    }
                    Label {
                        id: sumLabel
                        anchors.right: parent.right
                        anchors.rightMargin: Theme.paddingLarge
                        text: sum.toString()
                    }
                    menu: ContextMenu {
                        visible: !isFinished
                        MenuItem {
                            text: qsTr("Edit")
                            onClicked: {
                                goalReachedSum -= sum
                                var dialog = pageStack.push(Qt.resolvedUrl("AddGoalTransaction.qml"),
                                                        {goalId: id, goalTransactionId: id,
                                                                date: date, sum: sum});
                                dialog.accepted.connect (function () {
                                    dao.updateGoalTransaction(id, dialog.date.toISOString(), dialog.sum);
                                    listView.model.updateTransaction(model.index, dialog.date, dialog.sum,
                                                                     11, TransactionTypeEnum.Goal, "");
                                    pageStack.previousPage(pageStack.previousPage()).displayGoals();
                                    goalReachedSum += dialog.sum
                                });
                            }
                        }
                        MenuItem {
                            text: qsTr("Remove")
                            onClicked:  {
                                dao.removeTransaction(id);
                                goalReachedSum -= sum;
                                listView.model.remove(model.index);
                                pageStack.previousPage().displayGoals();
                            }
                        }
                    }
                }

                function displayTransactions() {
                    listView.model.clear();
                    dao.retrieveTransactionsByGoalId(goalId, function(transactions) {
                        for (var i = 0; i < transactions.length; i++) {
                            var transaction = transactions.item(i);
                            listView.model.addTransaction(transaction.id, new Date(transaction.date),
                                                          transaction.sum, transaction.category,
                                                          transaction.type);
                        }
                    });
                }

                Component.onCompleted: displayTransactions()
            }
        }
    }
}

