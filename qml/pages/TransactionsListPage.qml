import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../service"
import "../model"
import "../dialogs"

Page {
    property var transactionType
    id: page

    TransactionsListModel {
        id: transactionsListModel
    }

    Label {
        id: ifNoTransaction
        color: Theme.secondaryHighlightColor
        anchors.top: page.top
        anchors.topMargin: page.height / 2
        anchors.horizontalCenter: parent.horizontalCenter
    }
    SilicaListView {
        id: listView
        anchors.fill: parent
        currentIndex: 0
        PullDownMenu{
            MenuItem {
                text: qsTr("Change date interval")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("ChoiceDateIntervalDialog.qml"))
                    dialog.accepted.connect(function() {
                        dao.updateStatisticsValue();
                        listView.displayTransactions();
                    })
                }
            }
        }
        PushUpMenu {
            MenuItem {
                visible: transactionType === TransactionTypeEnum.Income || transactionType === undefined
                text: qsTr("Add income")
                onClicked:  {
                    var dialog = pageStack.push(Qt.resolvedUrl("AddIncome.qml"));
                    dialog.accepted.connect(function() {
                        listView.displayTransactions();
                    })
                }
            }
            MenuItem {
                visible: transactionType === TransactionTypeEnum.Expense || transactionType === undefined
                text: qsTr("Add expense")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("AddExpense.qml"));
                    dialog.accepted.connect(function() {
                        listView.displayTransactions();
                    })
                }
            }
        }
        VerticalScrollDecorator {}
        header: PageHeader {
            title: {
                if (transactionType === TransactionTypeEnum.Expense) {
                    qsTr("Expense")
                } else if (transactionType === TransactionTypeEnum.Income) {
                    qsTr("Income")
                } else {
                    qsTr("Transactions")
                }
            }
        }

        model: transactionsListModel
        delegate: ListItem {
            contentHeight: Theme.itemSizeMedium
            Column {
                Label {
                    id: transactionDate
                    text: (new Date(date)).toLocaleDateString(Locale.ShortFormat)
                    font.pixelSize: Theme.fontSizeExtraSmall
                    color: Theme.highlightColor
                    anchors {
                        left: parent.left
                        leftMargin: Theme.paddingLarge
                    }
                }
                Label {
                    id: categoryName
                    text: category.toString()
                    truncationMode: TruncationMode.Fade
                    width: page.width * 0.65
                    anchors {
                        left: parent.left
                        leftMargin: Theme.paddingLarge
                    }
                }
            }
            Label {
                id: sumLabel
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge
                verticalAlignment: Text.AlignVCenter
                height: parent.height
                text: if (type === TransactionTypeEnum.Income) {
                          sum.toString()
                      } else {
                          -sum.toString()
                      }
            }
            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Edit")
                    onClicked:  {
                        var dialog
                        if (type === TransactionTypeEnum.Income) {
                            dialog = pageStack.push(Qt.resolvedUrl("AddIncome.qml"), {incomeId: id});
                        } else {
                            dialog = pageStack.push(Qt.resolvedUrl("AddExpense.qml"),
                                                    {expenseId: id, expenseType: type})
                        }
                        dialog.accepted.connect (function () {
                            listView.displayTransactions()
                        })
                    }
                }
                MenuItem {
                    visible: !(type === TransactionTypeEnum.Goal ||
                             type === TransactionTypeEnum.FinishedGoal)
                    text: qsTr("Remove")
                    onClicked:  {
                        dao.removeTransaction(id);
                        listView.model.remove(model.index);
                        listView.displayTransactions()
                    }
                }
            }
        }

        function displayTransactions() {
            listView.model.clear();
            if(transactionType === undefined) {
                dao.retrieveTransactionsFromInterval(dao.startDate, dao.endDate, function(transactions) {
                    setupTransactionListModel(transactions)
                });
            } else {
                dao.retrieveTransactionsFromIntervalByType(
                            transactionType, dao.startDate, dao.endDate, function(transactions) {
                                setupTransactionListModel(transactions)
                            });
            }
        }

        function setupTransactionListModel(transactions) {
            ifNoTransaction.text = qsTr("")
             if(transactions.length !== 0) {
                for (var i = 0; i < transactions.length; i++) {
                    var transaction = transactions.item(i);
                    var category
                    dao.retrieveCategoryById(transaction.category_id, function(categoryObject) {
                        category = categoryObject.name;
                    });
                    listView.model.addTransaction(transaction.id, transaction.date,transaction.sum,
                                                  category, transaction.type, transaction.description);
                }
             } else {
                 if (transactionType === TransactionTypeEnum.Expense) {
                     ifNoTransaction.text = qsTr("You don't have any expenses right now");
                 } else if (transactionType === TransactionTypeEnum.Income) {
                     ifNoTransaction.text = qsTr("You don't have any incomes right now");
                 } else {
                     ifNoTransaction.text = qsTr("You don't have any operation right now");
                 }


            }
        }

        Component.onCompleted: displayTransactions()
    }
}

