import QtQuick 2.0
import Sailfish.Silica 1.0
import "../service"
import "../model"

Page {
    id: page

    SilicaListView {
        id: listView
        model: GoalListModel { id: goalsListModel }
        width: parent.width
        VerticalScrollDecorator {}
        height: parent.height
        header: PageHeader {
            title: qsTr("Finished goals")
            width: parent.width
        }
        Label {
            id: ifNoGoals
            color: Theme.secondaryHighlightColor
            y: page.height / 2
            anchors.horizontalCenter: parent.horizontalCenter
        }
        delegate: ListItem {
            Label {
                id: goalName
                text: name
                anchors.left: parent.left
                anchors.leftMargin: Theme.paddingLarge
                truncationMode: TruncationMode.Fade
                width: page.width * 0.65
            }
            Label {
                id: sumLabel
                text: reachedSum
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge
            }
            onClicked: {
                pageStack.push(Qt.resolvedUrl("GoalTransactionListPage.qml"),
                               {goalId: id, goalSum: sum, goalReachedSum: reachedSum,
                                   goalName: name, isFinished: true});
            }
        }

        function displayGoals() {
            listView.model.clear();
            dao.retrieveFinishedGoals(function(goals) {
                for (var i = 0; i < goals.length; i++) {
                    var goal = goals.item(i);
                    var reachedSum = 0;
                    dao.retrieveTransactionSumByGoalId(goal.id, function(result) {
                        if (result !== null) {
                            reachedSum = result;
                        }
                    })
                    listView.model.addGoal(goal.id, goal.name, goal.sum, reachedSum);
                }
                if(goals.length === 0) {
                    ifNoGoals.text = qsTr("You don't have any finished goals right now");
                }
            });
        }
        Component.onCompleted: displayGoals()
    }
}

