import QtQuick 2.0
import Sailfish.Silica 1.0
import "../"

Page {
    id: page

    SilicaFlickable {
        Column {
            PageHeader {
                title: qsTr("Settings")
            }
            id:column
            width: page.width
            spacing: Theme.paddingLarge
            Button {
                id: currency
                x: Theme.paddingLarge * 2.8
                text: qsTr("Currency")
                width: page.width/1.4
                color: Theme.primaryColor
                onClicked: pageStack.push(Qt.resolvedUrl("Currency.qml"))
           }
           Button {
                id: password
                x: Theme.paddingLarge * 2.8
                text: qsTr("Password")
                width: page.width/1.4
                color: Theme.primaryColor
                onClicked: pageStack.push(Qt.resolvedUrl("Password.qml"))
           }
        }
    }
}
