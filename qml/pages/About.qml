import QtQuick 2.0
import Sailfish.Silica 1.0
import "../"

Page {
    id: page

    Column {
        width: page.width
        spacing: Theme.paddingLarge

        PageHeader {
            title: qsTr("About")
        }
        Label {
            text: qsTr("Save Your Money is application which can help save your money by monitoring
                        income and expenditure. Also it can help to achieve finance goal.")
            wrapMode: Text.Wrap
            anchors {
                left: parent.left
                right: parent.right
                rightMargin: Theme.paddingLarge
                leftMargin: Theme.paddingLarge
            }
            horizontalAlignment: Text.horizontalCenter
        }
        Label {
            text: qsTr("Developers:")
            anchors {
                left: parent.left
                right: parent.right
                rightMargin: Theme.paddingLarge
                leftMargin: Theme.paddingLarge
            }
        }
        Label {
            text: qsTr("Roychikova Daria, Goncharov Alexander, Galkin Vladislav")
            wrapMode: Text.Wrap
            anchors {
                left: parent.left
                right: parent.right
                rightMargin: Theme.paddingLarge
                leftMargin: Theme.paddingLarge
            }
        }

        Label {
            text: qsTr("Yaroslavl 2016")
            anchors {
                left: parent.left
                right: parent.right
                rightMargin: Theme.paddingLarge
                leftMargin: Theme.paddingLarge
            }
            horizontalAlignment: Text.horizontalCenter
        }
        Label {
            text: qsTr("© Copyright")
            anchors {
                left: parent.left
                right: parent.right
                rightMargin: Theme.paddingLarge
                leftMargin: Theme.paddingLarge
            }
            horizontalAlignment: Text.horizontalCenter
        }
    }
}

