import QtQuick 2.0
import Sailfish.Silica 1.0
import harbour.save.your.money 1.0
import "../service"
import "../"
import "../dialogs"


Page {
    id: page

    onStatusChanged: {
        if (status == PageStatus.Active) {
            pageStack.pushAttached(Qt.resolvedUrl("GoalListPage.qml"))
        }
    }
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: statisticFieldColumn.height

        PullDownMenu{
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
            }
            MenuItem {
                text: qsTr("Change date interval")
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("ChoiceDateIntervalDialog.qml"))
                    dialog.accepted.connect(function() {
                        dao.updateStatisticsValue();
                    })
                }
            }/*
            MenuItem {
                text: qsTr("Настройки")
                onClicked: pageStack.push(Qt.resolvedUrl("Settings.qml"))
            }*/
        }
        PushUpMenu {
            MenuItem {
                text: qsTr("Add income")
                onClicked:  {
                    pageStack.push(Qt.resolvedUrl("AddIncome.qml"));
                }
            }
            MenuItem {
                text: qsTr("Add expense")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("AddExpense.qml"));
                }
            }
        }
        Column {
            id: statisticFieldColumn
            width: page.width
            spacing: Theme.paddingLarge * 2
            anchors {
                left:parent.left
                right: parent.right
                rightMargin: Theme.paddingLarge * 2
                leftMargin: Theme.paddingLarge * 2
            }
            PageHeader {
                x: Theme.paddingLarge * 2
                title: qsTr("Save Your Money")
            }
            Label {
                text: qsTr("Statistics:")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
            ValueButton {
                label: qsTr(dao.startDate.toLocaleDateString(Locale.ShortFormat) + " - " + dao.endDate.toLocaleDateString(Locale.ShortFormat))
                labelColor: Theme.secondaryHighlightColor
                onClicked: {
                    var dialog = pageStack.push(Qt.resolvedUrl("ChoiceDateIntervalDialog.qml"))
                    dialog.accepted.connect(function() {
                        dao.updateStatisticsValue();
                    })
                }

            }

            StatisticField {
                id: expenseStatisticField
                label: qsTr("Expense: ")
                ifShowButton: 1
                transactionType: TransactionTypeEnum.Expense
                clickPath: "pages/TransactionsListPage.qml"
                value: dao.expenseStatistics
            }
            StatisticField {
                id: incomeStatisticField
                label: qsTr("Income: ")
                ifShowButton: 1
                transactionType: TransactionTypeEnum.Income
                clickPath: "pages/TransactionsListPage.qml"
                value: dao.incomeStatistics
            }
            StatisticField {
                id: balanceStatisticField
                label: qsTr("Balance: ")
                ifShowButton: 0
                period: qsTr("For all time: ")
                value: dao.balanceStatistics
            }
            Button {
                id: viewAllTransactions
                text: qsTr("View all transactions")
                anchors {
                    left:parent.left
                    right: parent.right
                }
                color: Theme.primaryColor
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("TransactionsListPage.qml"));
                }
            }
        }
    }
}



