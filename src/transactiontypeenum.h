#ifndef TRANSACTIONTYPEENUM
#define TRANSACTIONTYPEENUM

#include <QObject>

class TransactionTypeEnum : public QObject {

    Q_OBJECT
    Q_ENUMS(TransactionType)

public:
    enum TransactionType {
        Expense = 0,
        Income = 1,
        Goal = 2,
        FinishedGoal = 3
    };

    TransactionTypeEnum(QObject *parent = 0) : QObject(parent) {}
};

#endif // TRANSACTIONTYPEENUM
