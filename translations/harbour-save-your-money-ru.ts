<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru" sourcelanguage="en">
<context>
    <name>About</name>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Roychikova Daria, Goncharov Alexander, Galkin Vladislav</source>
        <translation>Ройчикова Дарья, Гончаров Александр, Галкин Владислав</translation>
    </message>
    <message>
        <source>Yaroslavl 2016</source>
        <translation>Ярославль 2016</translation>
    </message>
    <message>
        <source>© Copyright</source>
        <translation>© Copyright</translation>
    </message>
    <message>
        <source>Developers:</source>
        <translation>Разработчики:</translation>
    </message>
    <message>
        <source>Save Your Money is application which can help save your money by monitoring
                        income and expenditure. Also it can help to achieve finance goal.</source>
        <translation>Save Your Money - приложение, помогающее сохранить ваши деньги путем контроля доходов и расходов, а также служит помощником в достижении финансовой цели.</translation>
    </message>
</context>
<context>
    <name>AddCategory</name>
    <message>
        <source>Add category</source>
        <translation>Добавление категории</translation>
    </message>
    <message>
        <source>New category</source>
        <translation>Новая категория</translation>
    </message>
</context>
<context>
    <name>AddExpense</name>
    <message>
        <source>Expense</source>
        <translation>Расходы</translation>
    </message>
    <message>
        <source>Sum</source>
        <translation>Сумма</translation>
    </message>
    <message>
        <source>Add a new category</source>
        <translation>Добавить новую категорию</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>Category</source>
        <translation>Категория</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
</context>
<context>
    <name>AddGoal</name>
    <message>
        <source>Add goal</source>
        <translation>Добавление цели</translation>
    </message>
    <message>
        <source>Name of goal</source>
        <translation>Название цели</translation>
    </message>
    <message>
        <source>Sum</source>
        <translation>Сумма</translation>
    </message>
    <message>
        <source>Edit goal</source>
        <translation>Редактировать цель</translation>
    </message>
</context>
<context>
    <name>AddGoalTransaction</name>
    <message>
        <source>Increase goal progress</source>
        <translation>Пополнение цели</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>Sum</source>
        <translation>Сумма</translation>
    </message>
</context>
<context>
    <name>AddIncome</name>
    <message>
        <source>Sum</source>
        <translation>Сумма</translation>
    </message>
    <message>
        <source>Add a new category</source>
        <translation>Добавить новую категорию</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <source>Category</source>
        <translation>Категория</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <source>Income</source>
        <translation>Доходы</translation>
    </message>
</context>
<context>
    <name>ChoiceDateIntervalDialog</name>
    <message>
        <source>Date interval</source>
        <translation>Временной интервал</translation>
    </message>
    <message>
        <source>Display for:</source>
        <translation>Отображение за:</translation>
    </message>
    <message>
        <source>Week</source>
        <translation>Неделю</translation>
    </message>
    <message>
        <source>Month</source>
        <translation>Месяц</translation>
    </message>
    <message>
        <source>Three months</source>
        <translation>3 месяца</translation>
    </message>
    <message>
        <source>Year</source>
        <translation>Год</translation>
    </message>
    <message>
        <source>Custom</source>
        <translation>Заданный пользователем</translation>
    </message>
    <message>
        <source>Start date</source>
        <translation>Начальная Дата</translation>
    </message>
    <message>
        <source>End date</source>
        <translation>Конечная Дата</translation>
    </message>
    <message>
        <source>For all time</source>
        <translation>За все время</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>The end date is later than a start date</source>
        <translation>Конечная дата позднее начальной даты</translation>
    </message>
</context>
<context>
    <name>Classification</name>
    <message>
        <source>Diagram</source>
        <translation>Диаграмма</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Balance:</source>
        <translation>Баланс:</translation>
    </message>
</context>
<context>
    <name>Currency</name>
    <message>
        <source>Currency</source>
        <translation>Валюта</translation>
    </message>
</context>
<context>
    <name>Dao</name>
    <message>
        <source>Food</source>
        <translation>Продукты</translation>
    </message>
    <message>
        <source>Car</source>
        <translation>Автомобиль</translation>
    </message>
    <message>
        <source>Cafe</source>
        <translation>Ресторан</translation>
    </message>
    <message>
        <source>Cellular</source>
        <translation>Связь</translation>
    </message>
    <message>
        <source>House</source>
        <translation>Жилье</translation>
    </message>
    <message>
        <source>Health</source>
        <translation>Здоровье</translation>
    </message>
    <message>
        <source>Others</source>
        <translation>Прочее</translation>
    </message>
    <message>
        <source>Salary</source>
        <translation>Зар. плата</translation>
    </message>
    <message>
        <source>Savings</source>
        <translation>Сбережения</translation>
    </message>
    <message>
        <source>Premium</source>
        <translation>Премия</translation>
    </message>
    <message>
        <source>Goal progress</source>
        <translation>Пополнение цели</translation>
    </message>
    <message>
        <source>Goal finish</source>
        <translation>Закрытие цели</translation>
    </message>
</context>
<context>
    <name>FinishedGoalListPage</name>
    <message>
        <source>Finished goals</source>
        <translation>Завершенные цели</translation>
    </message>
    <message>
        <source>You don&apos;t have any finished goals right now</source>
        <translation>У вас пока завершенных целей</translation>
    </message>
</context>
<context>
    <name>GoalListPage</name>
    <message>
        <source>Goals</source>
        <translation>Цели</translation>
    </message>
    <message>
        <source>Progress of all goals:</source>
        <translation>Прогресс по всем целям:</translation>
    </message>
    <message>
        <source>Add goal</source>
        <translation>Добавить цель</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <source>View finished goals</source>
        <translation>Посмотреть завершенные цели</translation>
    </message>
    <message>
        <source>You don&apos;t have any actual goals right now</source>
        <translation>У вас пока нет текущих целей</translation>
    </message>
</context>
<context>
    <name>GoalTransactionListPage</name>
    <message>
        <source>Add progress</source>
        <translation>Пополнить</translation>
    </message>
    <message>
        <source>Description of the goal</source>
        <translation>Описание цели</translation>
    </message>
    <message>
        <source>Saved</source>
        <translation>Отложено</translation>
    </message>
    <message>
        <source>from</source>
        <translation>из</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <source>Finish goal</source>
        <translation>Закрыть цель</translation>
    </message>
</context>
<context>
    <name>Password</name>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Currency</source>
        <translation>Валюта</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <source>Add income</source>
        <translation>Добавить доходы</translation>
    </message>
    <message>
        <source>Add expense</source>
        <translation>Добавить расходы</translation>
    </message>
    <message>
        <source>Save Your Money</source>
        <translation>Save Your Money</translation>
    </message>
    <message>
        <source>Statistics:</source>
        <translation>Статистика:</translation>
    </message>
    <message>
        <source>Expense: </source>
        <translation>Расходы:</translation>
    </message>
    <message>
        <source>Income: </source>
        <translation>Доходы:</translation>
    </message>
    <message>
        <source>Balance: </source>
        <translation>Баланс:</translation>
    </message>
    <message>
        <source>Change date interval</source>
        <translation>Изменить временной интервал</translation>
    </message>
    <message>
        <source>View all transactions</source>
        <translation>Посмотреть все операции</translation>
    </message>
    <message>
        <source>For all time: </source>
        <translation>За все время:</translation>
    </message>
</context>
<context>
    <name>TransactionsListPage</name>
    <message>
        <source>Expense</source>
        <translation>Расходы</translation>
    </message>
    <message>
        <source>Income</source>
        <translation>Доходы</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <source>Transactions</source>
        <translation>Все операции</translation>
    </message>
    <message>
        <source>Change date interval</source>
        <translation>Изменить временной интервал</translation>
    </message>
    <message>
        <source>Add income</source>
        <translation>Добавить доходы</translation>
    </message>
    <message>
        <source>Add expense</source>
        <translation>Добавить расходы</translation>
    </message>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>You don&apos;t have any expenses right now</source>
        <translation>У вас пока нет расходов</translation>
    </message>
    <message>
        <source>You don&apos;t have any incomes right now</source>
        <translation>У вас пока нет доходов</translation>
    </message>
    <message>
        <source>You don&apos;t have any operation right now</source>
        <translation>У вас пока нет операций</translation>
    </message>
</context>
</TS>
