<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Roychikova Daria, Goncharov Alexander, Galkin Vladislav</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yaroslavl 2016</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>© Copyright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Developers:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Your Money is application which can help save your money by monitoring
                        income and expenditure. Also it can help to achieve finance goal.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddCategory</name>
    <message>
        <source>Add category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New category</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddExpense</name>
    <message>
        <source>Expense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add a new category</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddGoal</name>
    <message>
        <source>Add goal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name of goal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit goal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddGoalTransaction</name>
    <message>
        <source>Increase goal progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sum</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddIncome</name>
    <message>
        <source>Income</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Category</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add a new category</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChoiceDateIntervalDialog</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Date interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Display for:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Three months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>For all time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Start date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>End date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The end date is later than a start date</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Classification</name>
    <message>
        <source>Diagram</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Balance:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Currency</name>
    <message>
        <source>Currency</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Dao</name>
    <message>
        <source>Food</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Car</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cafe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cellular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>House</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Health</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Others</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Salary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Savings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Premium</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Goal progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Goal finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FinishedGoalListPage</name>
    <message>
        <source>Finished goals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You don&apos;t have any finished goals right now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GoalListPage</name>
    <message>
        <source>View finished goals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add goal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Goals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Progress of all goals:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You don&apos;t have any actual goals right now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GoalTransactionListPage</name>
    <message>
        <source>Finish goal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description of the goal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Password</name>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Currency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change date interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add income</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add expense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save Your Money</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Statistics:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expense: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Income: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Balance: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>For all time: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View all transactions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TransactionsListPage</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Change date interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add income</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add expense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expense</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Income</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Transactions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You don&apos;t have any expenses right now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You don&apos;t have any incomes right now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You don&apos;t have any operation right now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
