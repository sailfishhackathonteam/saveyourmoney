# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-save-your-money

CONFIG += sailfishapp

SOURCES += src/harbour-save-your-money.cpp

OTHER_FILES += qml/harbour-save-your-money.qml \
    rpm/harbour-save-your-money.changes.in \
    rpm/harbour-save-your-money.spec \
    rpm/harbour-save-your-money.yaml \
    translations/*.ts \
    harbour-save-your-money.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-save-your-money-ru.ts

DISTFILES += \
    qml/service/Dao.qml \
    qml/pages/TransactionsListPage.qml \
    qml/pages/StartPage.qml \
    qml/pages/Settings.qml \
    qml/pages/Password.qml \
    qml/pages/Orientation.qml \
    qml/pages/GoalTransactionListPage.qml \
    qml/pages/GoalListPage.qml \
    qml/pages/FinishedGoalListPage.qml \
    qml/pages/Currency.qml \
    qml/pages/Classification.qml \
    qml/pages/ChoiceDateIntervalDialog.qml \
    qml/pages/AddIncome.qml \
    qml/pages/AddGoalTransaction.qml \
    qml/pages/AddGoal.qml \
    qml/pages/AddExpense.qml \
    qml/pages/AddCategory.qml \
    qml/pages/About.qml \
    qml/model/TransactionsListModel.qml \
    qml/model/GoalListModel.qml \
    qml/model/CategoriesListModel.qml \
    qml/cover/CoverPage.qml \
    qml/dialogs/AddCategory.qml \
    qml/dialogs/AddExpense.qml \
    qml/dialogs/AddGoal.qml \
    qml/dialogs/AddGoalTransaction.qml \
    qml/dialogs/AddIncome.qml \
    qml/dialogs/ChoiceDateIntervalDialog.qml

HEADERS += \
    src/transactiontypeenum.h

